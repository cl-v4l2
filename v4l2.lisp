;;;; Copyright 2009 Vitaly Mayatskikh <v.mayatskih@gmail.com>
;;;;
;;;; This file is a part of CL-Video4Linux2
;;;;
;;;; CL-Video4Linux2 is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; CL-Video4Linux2 is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :cl-v4l2)

(define-wrapper format ())

;; hack uint32->int32
(defun ioctl (fd req arg)
  ;; CFFI grovels C constants as int64, and that mangles V4L2 ioctl requests.
  ;; There was a need in a hack, which is commented out below. But current
  ;; IOLIB (0.7.1) handles mangled requests ok.
  ;;
  ;; (let ((req! (if (> req (ash 1 31))
  ;;                 (- req (ash 1 32))
  ;;                 req)))
    (isys:ioctl fd req arg))

(defun query-capabilities (fd)
  "Query for device capabilities."
  (let ((caps (make-instance 'capability)))
    (ioctl fd vidioc-querycap (capability-raw caps))
    caps))

(defun capable (caps cap)
  "Check if device supports given capability."
  (not (zerop (logand (capability-capabilities caps) cap))))

(defun get-stream-params (fd buf-type)
  "Get stream parameters."
  (let ((parms (make-instance 'streamparm :type buf-type)))
    (ioctl fd vidioc-g-parm (streamparm-raw parms))
    parms))

(defun get-tuner-params (fd idx)
  "Get tuner parameters."
  (let ((tuner (make-instance 'tuner :index idx)))
    (ioctl fd vidioc-g-tuner (tuner-raw tuner))
    tuner))

(defun get-input-params (fd idx)
  "Get input parameters."
  (let ((vidin (make-instance 'input :index idx)))
    (ioctl fd vidioc-enuminput (input-raw vidin))
    vidin))

(defun get-input-standard (fd idx)
  "Get input standard."
  (let ((std (make-instance 'standard :index idx)))
    (ioctl fd vidioc-enumstd (standard-raw std))
    std))

(defun get-format (fd idx)
  "Get pixel format."
  (let ((fmt (make-instance 'fmtdesc :index idx :type :buf-type-video-capture)))
    (ioctl fd vidioc-enum-fmt (fmtdesc-raw fmt))
    fmt))

(defun %device-info (caps)
  (cl:format nil "Driver: ~A~%Card: ~A~%Bus: ~A~%Version: ~A~%"
	     (capability-driver caps)
	     (capability-card caps)
	     (capability-bus-info caps)
	     (capability-version caps)))

(defun device-info (fd)
  "Get basic information about device."
  (let ((caps (query-capabilities fd)))
    (%device-info caps)))

(defun set-input (fd idx)
  "Set device input."
  (with-foreign-object (in :int)
    (setf (mem-ref in :int) idx)
    (ioctl fd vidioc-s-input in)))

(defun set-image-format (fd w h pixfmt)
  "Set dimensions and pixel format."
  (let ((format (make-instance 'format :type :buf-type-video-capture)))
    (with-slots (width height pixelformat field colorspace) (format-pix format)
      (setf width w
	    height h
	    pixelformat pixfmt
	    field :field-any
	    colorspace :colorspace-srgb))
    (ioctl fd vidioc-s-fmt (format-raw format))
    format))

(defun get-image-format (fd)
  "Get current format."
  (let ((format (make-instance 'format :type :buf-type-video-capture)))
    (ioctl fd vidioc-g-fmt (format-raw format))
    format))

(defun request-buffers (fd n map-type)
  "Request `n' buffers of type `map-type'."
  (with-foreign-object (req 'requestbuffers)
    (with-foreign-slots ((count type memory) req requestbuffers)
      (setf count n
	    type :buf-type-video-capture
	    memory map-type)
      (ioctl fd vidioc-reqbufs req)
      count)))

(defun query-buffer (fd n map-type)
  "Query buffer number `n' of type `map-type'."
  (let ((buf (make-instance 'buffer :index n
			    :type :buf-type-video-capture
			    :memory map-type)))
    (ioctl fd vidioc-querybuf (buffer-raw buf))
    buf))

(defun query-buffers (fd n map-type)
  "Query `n' buffers `n' of type `map-type'"
  (loop for buf from 0 below n
       collect (query-buffer fd buf map-type)))

(defun map-buffers (fd n)
  "Map `n' buffers into memory."
  (let* ((count (request-buffers fd n :memory-mmap))
	 (buffers (query-buffers fd count :memory-mmap)))
    (loop for buf in buffers
       collect
	 (with-slots (index length m) buf
	   (list buf
	    (isys:mmap (make-pointer 0) length isys:prot-read isys:map-shared fd
		       (buffer-union-offset m))
	    length)))))

(defun unmap-buffers (buffers)
  "Unmap given buffers from memory."
  (loop for buf in buffers do
       (multiple-value-bind (buffer addr length)
	   (values-list buf)
	 (declare (ignore buffer))
	 (isys:munmap addr length))))

(defun stream-action (fd req)
  (with-foreign-object (type 'buf-type 2) ;; 2 - hack to create `type' as pointer
    (setf (mem-ref type 'buf-type) :buf-type-video-capture)
    (ioctl fd req type)))

(defun stream-on (fd buffers)
  "Start video streaming."
  (loop for buffer in buffers do
       (ioctl fd vidioc-qbuf (buffer-raw (car buffer))))
  (stream-action fd vidioc-streamon))

(defun stream-off (fd)
  "Stop video streaming."
  (stream-action fd vidioc-streamoff))

(defun get-frame (fd)
  "Get next frame from device."
  (with-foreign-object (buf 'buffer)
    (with-foreign-slots ((index type memory) buf buffer)
      (setf type :buf-type-video-capture
	    memory :memory-mmap)
      (ioctl fd vidioc-dqbuf buf)
      (convert-from-foreign index :uint32))))

(defun put-frame (fd n)
  "Return frame buffer back to driver."
  (with-foreign-object (buf 'buffer)
    (with-foreign-slots ((index type memory) buf buffer)
      (setf index n
	    type :buf-type-video-capture
	    memory :memory-mmap))
    (ioctl fd vidioc-qbuf buf))
  n)

(defun set-control (fd ctrl-id level)
  "Set control to given level. Level should be in range of 0.0-1.0."
  (with-foreign-object (query 'queryctrl)
    (with-foreign-slots ((id minimum maximum) query queryctrl)
      (setf id ctrl-id)
      (ioctl fd vidioc-queryctrl query)
      (with-foreign-object (control 'control)
	(with-foreign-slots ((id value) control control)
	  (setf id ctrl-id
		value (+ minimum (round (* level (- maximum minimum))))))
        (ioctl fd vidioc-s-ctrl control)))))

(defun get-control (fd ctrl-id)
  "Get control level (in range of 0.0-1.0)."
  (with-foreign-object (query 'queryctrl)
    (with-foreign-slots ((id minimum maximum) query queryctrl)
      (setf id ctrl-id)
      (ioctl fd vidioc-queryctrl query)
      (with-foreign-object (control 'control)
	(with-foreign-slots ((id value) control control)
	  (setf id ctrl-id)
	  (ioctl fd vidioc-g-ctrl control)
	  (/ (- value minimum) (- maximum minimum)))))))
