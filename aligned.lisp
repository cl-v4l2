(include "linux/videodev2.h")

(in-package :cl-v4l2)

(cstruct format "struct v4l2_format"
  (type		"type"		:type buf-type)
  (pix		"fmt.pix"	:type pix-format)) ; ATM won't support anything except pix_format
