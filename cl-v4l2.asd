;; Copyright 2009 Vitaly Mayatskikh <v.mayatskih@gmail.com>
;;
;; This file is a part of CL-Video4Linux2
;;
;; CL-Video4Linux2 is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; CL-Video4Linux2 is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(cl:eval-when (:load-toplevel :execute)
  (asdf:operate 'asdf:load-op :cffi-grovel)
  (asdf:operate 'asdf:load-op :iolib.syscalls)
  (asdf:operate 'asdf:load-op :trivial-garbage)
  (asdf:operate 'asdf:load-op :closer-mop))

(defpackage #:cl-v4l2-asd
  (:use :cl :asdf))

(in-package #:cl-v4l2-asd)

(defsystem cl-v4l2
  :name "cl-video4linux2"
  :version "0.1"
  :author "Vitaly Mayatskikh <v.mayatskih@gmail.com>"
  :licence "GPLv3"
  :description "Video4Linux2 bindings"
  :serial t
  :components ((:file "package")
	       (cffi-grovel:grovel-file "grovel" :depends-on ("package"))
               (:file "videodev2" :depends-on ("grovel"))
	       (cffi-grovel:grovel-file "aligned" :depends-on ("videodev2"))
               (:file "v4l2" :depends-on ("videodev2"))))
